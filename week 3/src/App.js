import React, { useState } from 'react';
import LoginForm from './components/LoginForm';
import AddTodo from "./components/AddTodo";
import Todos from "./components/Todos";
import './App.css';

function App() {
    const adminUser ={
      email: "shivakumar.v@intimetec.com",
      password: "shivu123"
    }

    const [user, setUser] = useState({name: "", email: ""});
    const [error, setError] = useState("");

    const Login = details => {
      if (details.email == adminUser.email && details.password == adminUser.password){
        console.log("Logger in");
        setUser({
          name: details.name,
          email : details.email
        });
      }
      else{
        setError("Details do not match");
      }
    }

    const Logout = () => {
      setUser({ name:"", email:"" });
    }
    return(
      <div>
        {(user.email != "") ? (
          <div>
            <div className="logout">
            <button onClick={Logout}>Logout</button>
            </div>
            <div className="header">
            <h1>Todo App</h1>
            <h2>Welcome, <span>{user.name}</span></h2>
            </div>
            <div className="App">
            <AddTodo />
             <Todos />
             </div>
          </div>
        ) : (
          <LoginForm Login={Login} error={error} />
        )}
      </div>
    );
    
}

export default App;

// import React,{ useState } from 'react';

// // import Auto from './autoFocus';
// import './App.css';
// // import Counter from './counter';
// // import FriendStatus from './status.tsx' 

// function App() {
//   const [values, setValues] = useState({email: "",password:""});

//     const handleChange = event => {
//         const { name, value} = event.taget;

//         setValues({
//             ...values, 
//            [name]: value
//         })
//    }

//     const handleSubmit = event => {
//       event.preventDefault();
//     }

//     function submit(){
//       console.log('submitted Successfully');

//     }
//     return (
//         <div>
//       <form onSubmit={handleSubmit} noValidate>
//           <div>
//                 <label>Email</label>
//             <div>
//                 <input name="email" type="email" value={values.email} onChange={handleChange}/>
//            </div>
//            </div>
//            <div>
//                 <label>Password</label>
//             <div>
//                 <input name="password" type="password" value={values.password} onChange={handleChange}/>
//            </div>
//            </div>
//            <button type="submit">Submit</button>
//       </form>
//       </div>
//   );
// }
// export default App;
// import React from "react";
// import "./App.css";
// import AddTodo from "./components/AddTodo";
// import Todos from "./components/Todos";
// import loginform from "./components/form";

// function App() {
//   return (
//     <div>
//       <h1> Todo </h1>
//     <div className="App">
//       <loginform />
//       <AddTodo />
//       <Todos />
//     </div>
//     </div>
//   );
// }

// export default App;
