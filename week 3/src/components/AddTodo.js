import React, { useContext } from "react";
import { Context } from "./../Context";

function AddTodo() {
  const { addTodo, input, setInput } = useContext(Context);

  return (
    <div >
    <form id = "to-do-form" onSubmit={e => addTodo(e)}>
      <input
        type="text"
        placeholder="Type your todo here"
        onChange={e => setInput(e.target.value)}
        value={input}
      ></input>{'  '}
      <button type="submit">Add Todo</button>
    </form>
    </div>
  );
}

export default AddTodo;
