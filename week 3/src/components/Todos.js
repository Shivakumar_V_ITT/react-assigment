import React, { useContext } from "react";
import { Context } from "./../Context";
import TodoItem from "./TodoItem";

function Todos() {
  const { todos } = useContext(Context);

  const individualTodo = todos.map(todo => (
    <TodoItem key={todo.id} id={todo.id} task={todo.task} todo={todo} />
  ));

  return <div>{individualTodo}</div>;
}

export default Todos;
