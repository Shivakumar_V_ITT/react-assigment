import React, { useState, useEffect } from "react";
import firebase from "./firebase";

const Context = React.createContext();

function ContextProvider({ children }) {
  const [input, setInput] = useState("");
  const [todos, setTodos] = useState([]);
  const [edit, setEdit] = useState({
    id: null,
    value: ''
  });

  if (edit.id){
    // return <AddTodo edit={edit} />;
    setInput(edit.value);
    console.log(edit);
  }

  useEffect(() => {
    getTodos();
    console.log(todos);
  }, [ ]); // blank to run only on first launch

  function getTodos() {
    firebase.firestore().collection("todo").onSnapshot(function (querySnapshot) {
      setTodos(
        querySnapshot.docs.map((doc) => ({
          task: doc.data().todo,
          id: doc.id,
          completed: false,
        }
        ))
      );
    });
  }

  // Toggle complete

  function markComplete(id) {
    const updatedArr = todos.map(todo => {
      if (todo.id === id) {
        return { ...todo, completed: !todo.completed };
      }
      return todo;
    });
    setTodos(updatedArr);
  }

  // Delete todo item

  function editTodo(id, task) {
    setEdit({ id: id, value: task })
  }

  function deleteTodo(id) {
    firebase.firestore().collection("todo").doc(id).delete();
    const updatedArr = todos.filter(todo => todo.id !== id);
    setTodos(updatedArr);
  }

  // Add todo

  function addTodo(e) {
    e.preventDefault();
    console.log(todos);
    const id = todos.length ? todos[todos.length - 1].id + 1 : 0;
    if (input) {
      setTodos([...todos, { task: input, id, completed: false }]);
      firebase.firestore().collection("todo").add({
        id: (id+1),
        todo: input,
        completed: false,
      });
      setInput("");
    }
  }

  return (
    <Context.Provider
      value={{
        todos,
        setTodos,
        markComplete,
        deleteTodo,
        addTodo,
        editTodo,
        input,
        setInput
      }}
    >
      {children}
    </Context.Provider>
  );
}

export { ContextProvider, Context };
