import React, { useState } from 'react';
import LoginForm from './components/LoginForm';
import AddTodo from "./components/AddTodo";
import Todos from "./components/Todos";
import './App.css';
import firebase from "./firebase";
import { SettingsBackupRestoreRounded } from '@material-ui/icons';

function App() {
      
  const [error, setError] = useState();
    const [user, setUser] = useState({name: "", email: ""});
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();

    const ref = firebase.firestore().collection("users");

    function getAdminDetails() {
      ref.onSnapshot((querySnapshot) => {
        querySnapshot.forEach((doc)=>{
          setEmail(doc.data().email);
          setPassword(doc.data().password);
        });
      });
    }

    console.log(email);
    console.log(password);

    const Login = details => {
      getAdminDetails();
      if (details.email == email && details.password == password){
        console.log("Logger in");
        setUser({
          name: details.name,
          email : details.email
        });
      }
      else{
        setError("Details Do not match!");
      }
    }

    const Logout = () => {
      setUser({ name:"", email:"" });
      setError("");
    }
    return(
      <div>
        {(user && user.email != "") ? (
          <div>
            <div className="logout">
            <button onClick={Logout}>Logout</button>
            </div>
            <div className="header">
            <h1>Todo App</h1>
            </div>
            <div className="App">
            <AddTodo />
             <Todos />
             </div>
          </div>
        ) : (
          <LoginForm Login={Login} error={error} />
        )}
      </div>
    );
    
}

export default App;
