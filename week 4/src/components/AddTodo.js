import React, { useContext, useEffect, useRef } from "react";
import { Context } from "./../Context";

function AddTodo() {
  const { addTodo, input, setInput } = useContext(Context);
  const addtodo = useRef(null);

    useEffect(() => {
        addtodo.current.focus();
      });

  return (
    <div >
    <form id = "to-do-form" onSubmit={e => addTodo(e)}>
      <input
        type="text"
        placeholder="Type your todo here"
        onChange={e => setInput(e.target.value)}
        value={input}
        ref={addtodo}
      ></input>{'  '}
      <button type="submit">Add Todo</button>
    </form>
    </div>
  );
}

export default AddTodo;
