import React, { useState, useContext } from "react";
import { Context } from "./../Context";
import AddTodo from "./AddTodo"
import "./addtodo.css";
import DeleteIcon from '@material-ui/icons/Delete';
import { Checkbox } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';

  function TodoItem({ todo, task, id }) {
    const { markComplete, deleteTodo, editTodo } = useContext(Context);

    function getStyle() {
      return {
        textDecoration: todo.completed && "line-through"
      };
    }

    
    return (
      <div className="list" style={getStyle()}>
          <Checkbox id="check" onChange={() => markComplete(id)} style={{color: 'white', margin:'0.2em'}}/>
          <label style={{fontSize:'20px', margin:'0.2em'}}>{task}</label>
          <span style={{float:'right', margin:'0.9em'}}>
            <EditIcon className="edit" onClick={() => editTodo(id, task)} style={{ color: 'black' }} />
            <DeleteIcon className="delete" onClick={() => deleteTodo(id)} style={{ color: 'black' }} />
          </span>
      </div>
    );
  }

export default TodoItem;
