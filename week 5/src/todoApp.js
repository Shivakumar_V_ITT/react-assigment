import React, { useState } from 'react';
import LoginForm from './components/LoginForm';
import AddTodo from "./components/AddTodo";
import Todos from "./components/Todos";
import { useHistory } from 'react-router-dom';
import './App.css';
import firebase from "./components/firebase";

function TodoApp() {
      
    const [error, setError] = useState();
    const [user, setUser] = useState({id: "",name: "", email: ""});
    const history = useHistory();

    const ref = firebase.firestore().collection("users");

    const Login = details => {
      ref.onSnapshot((querySnapshot) => {
        querySnapshot.forEach((doc)=>{
            if (details.email == doc.data().email && details.password == doc.data().password){
              setUser({
                  id : doc.id,
                  name: doc.data().name,
                  email : doc.data().email,
              });
            }
            else{
              setError("Details Do not match!");
            }
          });
        });
      }

      localStorage.setItem('email',user.id);

    const Logout = () => {
      setUser({ name:"", email:"" });
      setError("");
      history.push('/');
    }

    const userP = () => {
      history.push('/user');
    }

    return(
      <div>
        {(user && user.email != "") ? (
          <div>
            <div className="logout">
            <button onClick={Logout}>Logout</button>
            <button onClick={userP}>Profile</button>
            </div>
            <div className="header">
            <h1>Todo App</h1>
            </div>
            <div className="App">
                  <AddTodo />
                  <Todos />
             </div>
          </div>
        ) : (
          <LoginForm Login={Login} error={error} />
        )}
      </div>
    );
    
}

export default TodoApp;
