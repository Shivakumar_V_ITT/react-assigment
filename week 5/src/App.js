import TodoApp from './todoApp';
import React from 'react';
import { BrowserRouter, Route, Switch, Router } from 'react-router-dom';
import SignUp from "./components/signUp";
import User from "./components/user";


function App(){

  return(
    <Switch>
      <Route path = "/" exact>
        <TodoApp />
      </Route>
      <Route path = "/register">
        <SignUp />
      </Route>
      <Route path = "/user">
        <User />
      </Route>
      </Switch>
  )
}

export default App;