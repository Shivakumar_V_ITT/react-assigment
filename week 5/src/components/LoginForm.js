import React, { useState } from 'react';
import "./loginForm.css";
import SignUp from "./signUp";
import { useHistory } from 'react-router-dom';
import ReactDOM from 'react-dom';

function LoginForm({ Login, error}) {
    const [details, setDetails] = useState({email: "", password: ""});
    const history = useHistory();

    const submitHandler = e =>{
        e.preventDefault();

        Login(details);
    }

    const signup = e =>{
        history.push('/register');
    }

    return (
        <form onSubmit={submitHandler}>
            <div className="form-inner">
                <h2>Login</h2>
                <div className="form-group">
                    <input type="email" placeholder="E-mail" name="email" id="email" onChange={e => setDetails({...details, email: e.target.value})} value={details.email}/>
                </div>
                <div className="form-group">
                    <input type="password" name="pasword" placeholder="Password" id="password" onChange={e => setDetails({...details, password: e.target.value})} value={details.password}/>
                    {(error != "") ? (<div className="error">{error}</div>):""}
                    <button value="LOGIN">LOGIN</button>
                </div>
                <div className="signup">
                    <button value="LOGIN" onClick={signup}>SIGN UP</button>
                </div>
                {/* <input className="button" type="submit" value="LOGIN" /> */}
            </div>
        </form>
    )
}

export default LoginForm;
