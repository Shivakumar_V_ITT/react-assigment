import React, { useContext, useEffect, useRef } from "react";
import { Context } from "./../Context";
import ReactTooltip from "react-tooltip";
import './App.css';

function AddTodo() {
  const { addTodo, input, setInput, edit} = useContext(Context);
  const addtodo = useRef('');

    useEffect(() => {
        addtodo.current.focus();
      });

  return (
    <div>
    <div>
    <form id = "to-do-form" onSubmit={e => addTodo(e)}>
      <input
        type="text"
        placeholder="Type your todo here"
        onChange={e => setInput(e.target.value)}
        value={input}
        ref={addtodo}
      ></input>{'  '}
      <button type="submit">Add Todo</button>
    </form>
    </div>
    </div>
  );
}

export default AddTodo;
