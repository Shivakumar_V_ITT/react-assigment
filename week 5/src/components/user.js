import logo from './background.jpg';
import React, { useState, useEffect } from 'react';
import './user.css';
import { useHistory } from 'react-router-dom';
import firebase from "./firebase";

function User() {
    const history = useHistory();
    const [details, setDetails] = useState({name: "", email: "", path: "",phone: "", country: "", city:""});

    useEffect(() => {
        const ref = firebase.firestore().collection("users");
        ref.onSnapshot((querySnapshot) => {
            querySnapshot.forEach((doc)=>{
                if (doc.id == localStorage.getItem('email')){
                  setDetails({
                    name: doc.data().name,
                    email : doc.data().email,
                    path: doc.data().path,
                    phone: doc.data().phone,
                    country: doc.data().country,
                    city: doc.data().city,
                  });
                }
              });
            });
        console.log(details);
      });

    function back(){
        history.push('/')
    }

    return(
    <div className='user'>
        <div className='profile'>
            <img src={details.path} alt="Logo" />
            <div className='details'>
            <h1>{details.name}</h1><br/><br/>
            <label>Email : {details.email} </label><br/><br/>
            <label>City :  {details.city}</label><br/><br/>
            <label>Country: {details.country}</label><br/><br/>
            <label>Phone: {details.phone} </label><br/><br/>
            </div>
        </div>
    </div>
    )
}

export default User;