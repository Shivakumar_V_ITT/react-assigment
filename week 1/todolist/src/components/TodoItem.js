import React, { useContext } from "react";
import { Context } from "./../Context";

function TodoItem({ todo, task, id }) {
  const { markComplete, deleteTodo } = useContext(Context);

  function getStyle() {
    return {
      textDecoration: todo.completed && "line-through"
    };
  }

  return (
    <div style={getStyle()}>
      <p>
        <input type="checkbox" onChange={() => markComplete(id)}></input>
        {task}
        <button onClick={() => deleteTodo(id)}>
          X
        </button>
      </p>
    </div>
  );
}

export default TodoItem;
