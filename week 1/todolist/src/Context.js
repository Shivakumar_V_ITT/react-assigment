import React, { useState } from "react";

const Context = React.createContext();

function ContextProvider({ children }) {
  const [input, setInput] = useState("");
  const [todos, setTodos] = useState([
    // { task: "Get a job", id: 1, completed: false },
    
  ]);

  // Toggle complete

  function markComplete(id) {
    const updatedArr = todos.map(todo => {
      if (todo.id === id) {
        return { ...todo, completed: !todo.completed };
      }
      return todo;
    });
    setTodos(updatedArr);
  }

  // Delete todo item

  function deleteTodo(id) {
    const updatedArr = todos.filter(todo => todo.id !== id);
    setTodos(updatedArr);
  }

  // Add todo

  function addTodo(e) {
    e.preventDefault();
    const id = todos.length ? todos[todos.length - 1].id + 1 : 0;
    if (input) {
      setTodos([...todos, { task: input, id, completed: false }]);
      setInput("");
    }
  }

  return (
    <Context.Provider
      value={{
        todos,
        setTodos,
        markComplete,
        deleteTodo,
        addTodo,
        input,
        setInput
      }}
    >
      {children}
    </Context.Provider>
  );
}

export { ContextProvider, Context };
