import React, { useState, useEffect } from 'react';
import './App.css';

const Counter = () => {
  const [count, setCount] = useState(0);

  // Similar to componentDidMount and componentDidUpdate:
  useEffect(() => {
    // Update the document title using the browser API
     document.title = `You clicked ${count} times`;
  });
  return (
    <div className="counter">
      <h1>Counter</h1>
      <h2>You clicked {count} times</h2>
      <button onClick={() => setCount(count + 1)}>
        +
      </button>{'  '}
      <button onClick={() => setCount(count - 1)}>
        -
      </button>{'\n'}
      <button onClick={() => setCount(count == 0)}>
        Reset
      </button> 
    </div>
  );
}

export default Counter;