import React, { useRef, useState, useEffect } from 'react';
import './App.css';

function Auto() {

  const emailE1 = useRef(null);

    useEffect(() => {
        emailE1.current.focus();
      });

  return (
    <div className="App">
      <form>
          <h2>Login In</h2>
          <label>Email</label>{' '}
        <input type="email" name="email" ref={emailE1}/><br/><br/>
        <label>Password</label>{' '}
        <input type="password" name="password" /><br/><br/>
        {' '}<button type="submit">Login</button>
      </form>
    </div>
  );
}

export default Auto;
