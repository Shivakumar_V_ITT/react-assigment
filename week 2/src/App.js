import { useState, useEffect } from 'react';
import useFriendStatus from './status.tsx';
import Auto from './autoFocus'
import './App.css';
import Counter from './counter';
import FriendStatus from './status.tsx' 

function App() {
  const friendList = [
    { id: 1, name: 'Phoebe', status : 'online'},
    { id: 2, name: 'Rachel', status: 'offline'},
    { id: 3, name: 'Ross', status: 'online'}, 
  ];

  return (
    <div className='auto'>
    <Auto />
    <Counter />
    <FriendStatus />
    </div>

    // <>
    //   <li style={color: friendList.status === "online" ? "green" : "red"}>
    //     {friendList.map(friend => (
    //       <option key={friend.id} value={friend.id}>
    //         {friend.name} {friend.status}
    //       </option>
    //     ))}
    //   </li>
    // </>
  );
        }
export default App;