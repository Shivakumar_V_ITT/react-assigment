import React, { useState, useEffect } from 'react';
import './App.css';

const FriendStatus = () => {
    const friendList = [
      { id: 1, name: 'Phoebe' },
      { id: 2, name: 'Rachel' },
      { id: 3, name: 'Ross' },
    ];
  
    return (
      <div className="status">
          <h1>Friend Status</h1>
          <h5>
        <ul>
        {friendList.map(friend => (
            <option key={friend.id} value={friend.id} >
              {friend.name} {'-'} {localStorage.getItem(JSON.stringify(friend.id))}
            </option>
          ))}
        </ul>
        </h5>
      </div>
    );
  }
  
  export default FriendStatus;