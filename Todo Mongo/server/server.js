const express = require("express");
const mongoose = require("mongoose");

const app = express();
app.use(express.json());

const db = "mongodb://localhost:27017/Todo"

mongoose.connect(db, ({useNewUrlParser: true, useUnifiedTopology: true}))
.then(console.log("connected to mongoDB"))
.catch(err=> console.log(err))

const todoSchema = new mongoose.Schema({
    task: {type: String},
    complete: {
        type: Boolean,
        default: false
    }
})

const Todo = mongoose.model('todo', todoSchema)

app.get("/todos/post", (req, res) => {
    Todo.find().then(todo => res.json(todo))
})

app.post("/todos/post", (req, res) => {
    const newTodo = new Todo({
        task: req.body.task
    })
    newTodo.save().then(todo => res.json(todo))
})

app.delete("/todos/post/:id", (req, res) => {
    Todo.findByIdAndDelete(req.params.id)
    .then(()=> res.json({remove : true}))
})

app.put("/todos/post/:id", (req, res, next) => {
    const newTodo = new Todo({
      _id: req.params.id,
      task: req.body.task,
    });
    Todo.updateOne({_id: req.params.id}, newTodo).then(
      () => {
        res.status(201).json({
          message: 'updated successfully!'
        });
      }
    ).catch(
      (error) => {
        res.status(400).json({
          error: error
        });
      }
    );
  });

app.listen(5000, ()=>{
    console.log("server is running at port 5000")
})