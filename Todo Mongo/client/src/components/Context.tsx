import axios from "axios";
import React, { useState, useEffect } from "react";
import firebase from "./firebase";

const Context = React.createContext<any>("");

interface IProps {
  children: any;
  // any other props that come into the component
}

function ContextProvider({ children }: IProps) {
  const [input, setInput] = useState("");
  const [todos, setTodos] = React.useState<any>([]);
  const [edit, setEdit] = useState(false);
  const [inputId, setInputId] = useState("");

  useEffect(() => {
    getTodos();
  }, []); // blank to run only on first launch

  function getTodos() {
    axios.get("/todos/post").then((res) => {
      setTodos(res.data);
      console.log(todos);
      console.log("called");
    });
  }

  function markComplete(id: any) {
    const updatedArr = todos.map((todo: any) => {
      if (todo._id === id) {
        return { ...todo, completed: !todo.completed };
      }
      return todo;
    });
    setTodos(updatedArr);
  }

  function editTodo(id: any, task: any) {
    console.log(id);
    setInputId(id);
    setInput(task);
    setEdit(true);
  }

  function deleteTodo(id: any) {
    console.log(id);
    axios.delete("/todos/post/" + id).then((res) => getTodos());
  }

  // Add todo

  function addTodo(e: any) {
    e.preventDefault();
    const id = todos.length ? todos[todos.length - 1].id + 1 : 0;
    if (inputId && edit) {
      const task = { task: input };
      axios.put("/todos/post/" + inputId, task).then((res) => {
        console.log(res.data);
        getTodos();
      });
      setInput("");
      setInputId("");
      setEdit(false);
    }
    if (input && !inputId && !edit) {
      const task = { task: input };
      axios
        .post("/todos/post", task)
        .then((res) => {
          console.log("Data send");
          getTodos();
        })
        .catch((err) => console.log(err.data));
      setInput("");
    }
  }

  return (
    <Context.Provider
      value={{
        todos,
        setTodos,
        markComplete,
        deleteTodo,
        addTodo,
        editTodo,
        edit,
        input,
        setInput,
      }}
    >
      {children}
    </Context.Provider>
  );
}

export { ContextProvider, Context };
